package edu.buffalo.cse.cse486586.simpledht;

import android.database.MatrixCursor;

import java.io.Serializable;

/**
 * Created by tarun on 3/27/15.
 */
public class Packet implements Serializable {
    private PacketType pType;
    private String key;
    private String value;
    private String keyHash;
    private String srcPort;
    private String destPort;
    private String srcSHACode;
    private String predSHACode;
    private String succSHACode;
    private String firstNodeSHACode;
    private String SHAMessage;
    private String succPort;
    private String predPort;
    private String replyString;
    private int numMachines;

    public void setNumMachines(int numMachines) {
        this.numMachines = numMachines;
    }
    public int getNumMachines() {
        return numMachines;
    }

    public void setReplyString(String replyString) {
        this.replyString = replyString;
    }
    public String getReplyString() {
        return replyString;
    }

    public void setPredPort(String predPort) {
        this.predPort = predPort;
    }
    public void setSuccPort(String succPort) {
        this.succPort = succPort;
    }
    public String getPredPort() {
        return predPort;
    }
    public String getSuccPort() {
        return succPort;
    }

    public void setKeyHash(String keyHash) {
        this.keyHash = keyHash;
    }
    public String getKeyHash() {
        return keyHash;
    }

    public void setValue(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }

    public void setFirstNodeSHACode(String firstNodeSHACode) {
        this.firstNodeSHACode = firstNodeSHACode;
    }

    public String getFirstNodeSHACode() {
        return firstNodeSHACode;
    }

    Packet(PacketType pType, String key, String srcPort, String destPort, String srcSHACode) {
        this.pType = pType;
        this.key = key;
        this.srcPort = srcPort;
        this.destPort = destPort;
        this.srcSHACode = srcSHACode;
    }

    Packet(){}

    Packet(String key, String value, String keyHash){
        this.key = key;
        this.value = value;
        this.keyHash = keyHash;
    }

    public void setSrcPort(String srcPort) {
        this.srcPort = srcPort;
    }
    public String getSrcPort() {
        return srcPort;
    }

    public void setDestPort(String destPort) {
        this.destPort = destPort;
    }
    public String getDestPort() {
        return destPort;
    }

    public void setKey(String key) {
        this.key = key;
    }
    public String getKey() {
        return key;
    }

    public void setpType(PacketType pType) {
        this.pType = pType;
    }
    public PacketType getpType() {
        return pType;
    }

    public void setSrcSHACode(String srcSHACode) {
        this.srcSHACode = srcSHACode;
    }
    public String getSrcSHACode() {
        return srcSHACode;
    }

    public void setPredSHACode(String predSHACode) {
        this.predSHACode = predSHACode;
    }
    public void setSuccSHACode(String succSHACode) {
        this.succSHACode = succSHACode;
    }

    public String getPredSHACode() {
        return predSHACode;
    }
    public String getSuccSHACode() {
        return succSHACode;
    }
}


enum PacketType {
    GlobalDelete, GlobalQuery, GlobalQueryAck, ChordJoinRequest, ChordJoinAck, Insert_Req, Query_Req, Query_Ack, mcUpdate

}