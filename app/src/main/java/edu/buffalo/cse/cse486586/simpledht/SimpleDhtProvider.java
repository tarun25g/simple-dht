package edu.buffalo.cse.cse486586.simpledht;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Formatter;
import java.util.List;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;

public class SimpleDhtProvider extends ContentProvider {
    static final String TAG = SimpleDhtProvider.class.getSimpleName();
    private String myPort;
    private String myPortSHA;
    private Node sNode = new Node();
    static final int SERVER_PORT = 10000;
    private List<Node> chordNodes = new ArrayList<>();
    private MatrixCursor mcurr;
    Boolean isQueryAcknoweleged;
    Boolean isGlobalQueryAcknoweleged;
    private MatrixCursor mGlobalCurr;
    int nMachines=1;
    int countMc = 0;
    public String remotePort[] = {"11108", "11112", "11116", "11120", "11124"};

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // TODO Auto-generated method
        if (selection.equals("@")) {
            // delete local files
            File baseDir = new File(System.getProperty("user.dir") + "data/data/edu.buffalo.cse.cse486586.simpledht/files");
            File[] allFiles = baseDir.listFiles();

            for (File f : allFiles) {
                if (!f.delete()) return -1;
            }
            return 0;
        } else if (selection.equals("*")) {
            System.out.println("Global Delete Request");
            // send message to all nodes to delete global files
            System.out.println("");
            Packet msg = new Packet();
            msg.setKey("*");
            msg.setpType(PacketType.GlobalDelete);
            //msg.setKey("@");
            Socket socket;
            try {
                for (String port : remotePort) {
                    System.out.println("Global Delete Packet sent to " + port);
                    socket = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}), Integer.parseInt(port));
                    ObjectOutputStream ost = new ObjectOutputStream(socket.getOutputStream());
                    ost.writeObject(msg);
                    socket.close();
                }
            } catch (UnknownHostException e) {
                //Log.e(TAG, "ClientTask UnknownHostException");
                System.out.println("ClientTask UnknownHostException");

            } catch (IOException e) {
                System.out.println("ClientTask socket IOException..");
                //Log.e(TAG, "ClientTask socket IOException..");
            }
            return 0;
        } else {
            System.out.println("Global Delete Request");
            // send message to all nodes to delete global files
            System.out.println("");
            Packet msg = new Packet();
            msg.setpType(PacketType.GlobalDelete);
            msg.setKey(selection);
            Socket socket;
            try {
                for (String port : remotePort) {
                    System.out.println("Global Delete Packet sent to " + port);
                    socket = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}), Integer.parseInt(port));
                    ObjectOutputStream ost = new ObjectOutputStream(socket.getOutputStream());
                    ost.writeObject(msg);
                    socket.close();
                }
            } catch (UnknownHostException e) {
                //Log.e(TAG, "ClientTask UnknownHostException");
                System.out.println("ClientTask UnknownHostException");

            } catch (IOException e) {
                System.out.println("ClientTask socket IOException..");
                //Log.e(TAG, "ClientTask socket IOException..");
            }
            return 0;
        }
    }
    @Override
    public String getType(Uri uri) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        String key = values.getAsString("key");
        String value = values.getAsString("value");


        if (sNode.getPort().equals(sNode.getPredecessorPort()) && sNode.getPort().equals(sNode.getSuccessorPort())) {
            try {
                FileOutputStream outputStream = getContext().openFileOutput(key, Context.MODE_PRIVATE);
                outputStream.write(value.getBytes());
                System.out.println("Sending data ..............." + value);
                outputStream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Log.v("insert", values.toString());
            return uri;
        }


        String keySHA = "";
        try {
            keySHA = genHash(key);
        } catch (NoSuchAlgorithmException e) {
            System.out.println(e.getMessage());
        }
        RingComparator keyComp = new RingComparator();
        System.out.println("Started At "+sNode.getPort()  + " Compare(keySHA, sNodeSHA) gives " + keyComp.compare(keySHA, sNode.getSHA()) + " and keyComp.compare(keySHA, sNode.getPredecessorNode()) gives " + keyComp.compare(keySHA, sNode.getPredecessorNode()));

        if ((keySHA.equals(sNode.getSHA())) ||
                (keyComp.compare(keySHA, sNode.getSHA()) == -1 && sNode.getIsFirstNode()) ||
                (keyComp.compare(keySHA, sNode.getPredecessorNode()) == 1 && sNode.getIsFirstNode()) ||
                (keyComp.compare(keySHA, sNode.getSHA()) == -1 && (keyComp.compare(keySHA, sNode.getPredecessorNode() )==1))) {
            try {
                System.out.println("Inserting in First Node -"+ key+"-"+value+"-"+keySHA+" at "+sNode.getPort());
                FileOutputStream outputStream = getContext().openFileOutput(key, Context.MODE_PRIVATE);
                outputStream.write(value.getBytes());
                outputStream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //Log.v("insert", values.toString());
        }
        else {
            System.out.println ("Message Not inserted Locally. Now forwarding " + key+"-" + value + " to sucessor-" + sNode.getSuccessorPort());
            new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "INSERT_REQ", key, value, keySHA, sNode.getSuccessorPort());
        }

        return uri;
    }

    void insertNode(Node nNode) {
        chordNodes.add(nNode);
        Collections.sort(chordNodes, new RingComparator());
        for (int i=0;i<chordNodes.size();i++) {
            if (i == 0)chordNodes.get(i).setIsFirstNode(true);
            else chordNodes.get(i).setIsFirstNode(false);

            if (i!=0) {
                chordNodes.get(i).setPredecessorPort(chordNodes.get(i-1).getPort());
            }
            else {
                chordNodes.get(i).setPredecessorPort(chordNodes.get(chordNodes.size()-1).getPort());
            }

            if (i == chordNodes.size()-1) {
                chordNodes.get(i).setSuccessorPort(chordNodes.get(0).getPort());
            }
            else {
                chordNodes.get(i).setSuccessorPort(chordNodes.get(i+1).getPort());
            }
        }

    }

    @Override
    public boolean onCreate() {
        // TODO Auto-generated method stub
        TelephonyManager tel = (TelephonyManager) this.getContext().getSystemService(Context.TELEPHONY_SERVICE);
        String portStr = tel.getLine1Number().substring(tel.getLine1Number().length() - 4);
        myPort = String.valueOf((Integer.parseInt(portStr) * 2));
        isQueryAcknoweleged = false;
        isGlobalQueryAcknoweleged = false;
        try {
            ServerSocket serverSocket = new ServerSocket(SERVER_PORT);
            new ServerTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, serverSocket);
        } catch (IOException e) {
            //Log.e(TAG, "Can't create a ServerSocket");
            System.out.println("Can't create a ServerSocket");
        }

        try {
            myPortSHA = genHash(Integer.toString((Integer.valueOf(myPort))/2));
        } catch (NoSuchAlgorithmException e) {
            System.out.println("No Such Algorithm on " + myPort);
        }

        sNode.setPort(myPort);
        sNode.setSHA(myPortSHA);
        sNode.setPredecessorNode(myPortSHA);
        sNode.setSuccessorNode(myPortSHA);
        sNode.setPredecessorPort(myPort);
        sNode.setSuccessorPort(myPort);

        System.out.println("My port is " + myPort);
        if (!myPort.equals("11108")) {
            System.out.println("I am " + myPort);
            new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "JOIN_REQ", myPort, "11108", myPortSHA);
        }
        else {
            System.out.println("I am 11108");
            sNode.setIsFirstNode(true);
            chordNodes.add(sNode);
        }
        return false;
    }

    private class ClientTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... msgs) {
            if (msgs[0].equals("JOIN_REQ")) {
                System.out.println("CLIENT: In Client.. InBackground Thread..Port-" + myPort);
                System.out.println("Sending Join Req - " + msgs[1] + "-" + msgs[2] + "-" + msgs[3]);
                Socket socket;
                try {
                    socket = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}), 11108);
                    Packet msg = new Packet(PacketType.ChordJoinRequest, "", msgs[1], msgs[2], msgs[3]);

                    ObjectOutputStream ost = new ObjectOutputStream(socket.getOutputStream());
                    ost.writeObject(msg);
                    ost.flush();
                    ost.close();
                    socket.close();
                    System.out.println("CLIENT: First Packet Sent.");
                } catch (UnknownHostException e) {
                    //Log.e(TAG, "ClientTask UnknownHostException");
                    System.out.println("ClientTask UnknownHostException");

                } catch (IOException e) {
                    System.out.println("ClientTask socket IOException........e.toString()....." + e.toString());
                    //Log.e(TAG, "ClientTask socket IOException..");
                }
            }
            else if (msgs[0].equals("QUERY_REQ")) {
                System.out.println("In CLIENT Task: Sending Query Request to Successor from " + myPort);
                System.out.println("Sending Query Req - " + msgs[1] + "-" + msgs[2] + "-" + msgs[3]+"-" + msgs[4]);//+ msgs[5]);
                Socket socket;

                try {
                    socket = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}), Integer.parseInt(msgs[4]));
                    Packet msg = new Packet();
                    msg.setKey(msgs[1]);
                    msg.setKeyHash(msgs[2]);
                    msg.setSrcPort(msgs[3]);
                    msg.setSuccPort(msgs[4]);
                    msg.setpType(PacketType.Query_Req);
                    ObjectOutputStream ost = new ObjectOutputStream(socket.getOutputStream());
                    ost.writeObject(msg);
                    ost.flush();
                    ost.close();
                    socket.close();
                    System.out.println("CLIENT: Message Forwarded.");
                } catch (UnknownHostException e) {
                    //Log.e(TAG, "ClientTask UnknownHostException");
                    System.out.println("ClientTask UnknownHostException");

                } catch (IOException e) {
                    System.out.println("ClientTask socket IOException........e.toString()....." + e.toString());
                    //Log.e(TAG, "ClientTask socket IOException..");
                }


            }

            else if (msgs[0].equals("DELETE_REQ")) {
                // broadcast delete to all
            }



            else if (msgs[0].equals("INSERT_REQ")) {
                System.out.println("CLIENT: Sending Insert Request to Successor from " + myPort);
                System.out.println("Sending Insert Req - " + msgs[1] + "-" + msgs[2] + "-" + msgs[3]+"-" + msgs[4]);
                Socket socket;

                try {
                    socket = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}), Integer.parseInt(msgs[4]));
                    Packet msg = new Packet(msgs[1], msgs[2], msgs[3]);
                    msg.setpType(PacketType.Insert_Req);
                    ObjectOutputStream ost = new ObjectOutputStream(socket.getOutputStream());
                    ost.writeObject(msg);
                    ost.flush();
                    ost.close();
                    socket.close();
                    System.out.println("CLIENT: Message Forwarded.");
                } catch (UnknownHostException e) {
                    //Log.e(TAG, "ClientTask UnknownHostException");
                    System.out.println("ClientTask UnknownHostException");

                } catch (IOException e) {
                    System.out.println("ClientTask socket IOException........e.toString()....." + e.toString());
                    //Log.e(TAG, "ClientTask socket IOException..");
                }
            }
            return null;
        }
    }

    int updateNumMachines() {
        Packet msg = new Packet();
        msg.setpType(PacketType.mcUpdate);
        msg.setNumMachines(nMachines);
        Socket socket;
        try {
            for (String port : remotePort) {
                System.out.println("Global Delete Packet sent to " + port);
                socket = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}), Integer.parseInt(port));
                ObjectOutputStream ost = new ObjectOutputStream(socket.getOutputStream());
                ost.writeObject(msg);
                socket.close();
            }
        } catch (UnknownHostException e) {
            //Log.e(TAG, "ClientTask UnknownHostException");
            System.out.println("ClientTask UnknownHostException");

        } catch (IOException e) {
            System.out.println("ClientTask socket IOException..");
            //Log.e(TAG, "ClientTask socket IOException..");
        }
        return 0;
    }

    private class ServerTask extends AsyncTask<ServerSocket, String, Void> {
        @Override
        protected Void doInBackground(ServerSocket... sockets) {
            ServerSocket serverSocket = sockets[0];
            Socket ss;
            try {
                while (true) {
                    System.out.println("Server: Started..");
                    ss = serverSocket.accept();
                    ObjectInputStream in = new ObjectInputStream(ss.getInputStream());
                    Packet msg = (Packet) in.readObject();
                    System.out.println("Received Packet Request type is -"+msg.getpType());
                    in.close();
                    if (msg.getpType() == PacketType.ChordJoinRequest) {
                        nMachines++;
                        System.out.println("Join Req From-" + msg.getSrcPort()+"-"+ /* " " + msg.getDestPort()*/  " " + msg.getSrcSHACode());
                        Node nd = new Node();
                        nd.setPort(msg.getSrcPort());
                        nd.setSHA(msg.getSrcSHACode());
                        insertNode(nd);

                        int sucId=0, preId=0, i;
                        for (i=0; i<chordNodes.size(); i++) {
                            if (chordNodes.get(i).getSHA().equals(nd.getSHA())) {
                                if (i == chordNodes.size()-1)
                                    sucId = 0;
                                else
                                    sucId = i+1;
                                if (i == 0)
                                    preId = chordNodes.size()-1;
                                else
                                    preId = i-1;
                                break;
                            }
                        }

                        Node sucNode = chordNodes.get(sucId);
                        Node preNode = chordNodes.get(preId);
                        nd.setPredecessorNode(sucNode.getPredecessorNode());
                        nd.setSuccessorNode(preNode.getSuccessorNode());
                        sucNode.setPredecessorNode(nd.getSHA());
                        preNode.setSuccessorNode(nd.getSHA());
                        chordNodes.set(i, nd);
                        chordNodes.set(preId, preNode);
                        chordNodes.set(sucId, sucNode);

                        System.out.println("Now printing Chord Nodes");
                        for (int k = 0; k < chordNodes.size(); k++) {
                            System.out.println("Chord Nodes are - " + chordNodes.get(k).getPort()+"-"+chordNodes.get(k).getSuccessorNode()+"-"+chordNodes.get(k).getPredecessorNode()+"-"+chordNodes.get(k).getSHA()+"-"+chordNodes.get(k).getSuccessorPort()+"-"+chordNodes.get(k).getPredecessorPort());
                        }
                        Packet ackMsg = new Packet(PacketType.ChordJoinAck, "", "11108", "", "");
                        ackMsg.setFirstNodeSHACode(chordNodes.get(0).getSHA());
                        updateNumMachines();
                        //ackMsg.setNumMachines(nMachines);
                        Socket socket;
                        ObjectOutputStream ost;

                        try {
                            System.out.println("Server: From Server, Sending Data to - " + preNode.getPort() + " of type " + msg.getpType() + " from " + ackMsg.getSrcPort());
                            socket = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}), Integer.parseInt(preNode.getPort()));
                            ost = new ObjectOutputStream(socket.getOutputStream());
                            ackMsg.setDestPort(preNode.getPort());
                            ackMsg.setPredSHACode(preNode.getPredecessorNode());
                            ackMsg.setSuccSHACode(preNode.getSuccessorNode());
                            ackMsg.setSuccPort(preNode.getSuccessorPort());
                            ackMsg.setPredPort(preNode.getPredecessorPort());
                            ost.writeObject(ackMsg);
                            ost.reset();

                            // System.out.println("Server: From Server, Sending Data to - " + msg.getClientId() + " of type " + msg.getMessageType() + " from " + msg.getServerId());
                            socket = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}), Integer.parseInt(nd.getPort()));
                            ost = new ObjectOutputStream(socket.getOutputStream());
                            ackMsg.setDestPort(nd.getPort());
                            ackMsg.setPredSHACode(nd.getPredecessorNode());
                            ackMsg.setSuccSHACode(nd.getSuccessorNode());
                            ackMsg.setSuccPort(nd.getSuccessorPort());
                            ackMsg.setPredPort(nd.getPredecessorPort());
                            ost.writeObject(ackMsg);
                            ost.reset();

                            // System.out.println("Server: From Server, Sending Data to - " + msg.getClientId() + " of type " + msg.getMessageType() + " from " + msg.getServerId());
                            socket = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}), Integer.parseInt(sucNode.getPort()));
                            ost = new ObjectOutputStream(socket.getOutputStream());
                            ackMsg.setDestPort(sucNode.getPort());
                            ackMsg.setPredSHACode(sucNode.getPredecessorNode());
                            ackMsg.setSuccSHACode(sucNode.getSuccessorNode());
                            ackMsg.setSuccPort(sucNode.getSuccessorPort());
                            ackMsg.setPredPort(sucNode.getPredecessorPort());
                            ost.writeObject(ackMsg);
                            ost.reset();
                            socket.close();
                        } catch (UnknownHostException e) {
                            System.out.println("\nCLIENT: Unknown host exception in socket Client-proposal..\n");
                            //Log.e(TAG, "ClientTask UnknownHostException");
                        } catch (IOException e) {
                            System.out.println("\nCLIENT: IO Exception in Socket client proposal..\n");
                            //Log.e(TAG, "ClientTask socket IOException. Here");
                        }
                        continue;
                    }
                    if (msg.getpType() == PacketType.ChordJoinAck) {
                        sNode.setPredecessorNode(msg.getPredSHACode());
                        sNode.setSuccessorNode(msg.getSuccSHACode());
                        sNode.setSuccessorPort(msg.getSuccPort());
                        sNode.setPredecessorPort(msg.getPredPort());
                        //nMachines=msg.getNumMachines();
                        if (sNode.getSHA().equals(msg.getFirstNodeSHACode())) {
                            sNode.setIsFirstNode(true);
                        }
                        else sNode.setIsFirstNode(false);
                        System.out.println("In chord join ACK - " + sNode.getPort()+"-"+sNode.getSHA()+"-"+sNode.getPredecessorNode()+"-"+sNode.getSuccessorNode() + " and my isFirstNodeStatus is " + sNode.getIsFirstNode() +"-"+sNode.getPredecessorPort()+"-"+sNode.getSuccessorPort());
                        continue;
                    }
                    else if (msg.getpType() == PacketType.mcUpdate) {
                        nMachines = msg.getNumMachines();
                        System.out.println("Reaching Here and total number of machines now are "+nMachines);
                        continue;
                    }
                    else if (msg.getpType() == PacketType.Insert_Req) {
                        String keySHA = msg.getKeyHash();

                        System.out.println("Insert Request at-"+ sNode.getPort()+"from-"+sNode.getPredecessorPort());
                        RingComparator keyComp = new RingComparator();
                        System.out.println("At "+sNode.getPort()  + " Compare(keySHA, sNodeSHA) gives " + keyComp.compare(keySHA, sNode.getSHA()) + " and keyComp.compare(keySHA, sNode.getgetPredecessorSHA()) gives " + keyComp.compare(keySHA, sNode.getSHA()));

                        if ((keySHA.equals(sNode.getSHA())) ||
                                (keyComp.compare(keySHA, sNode.getSHA()) == -1 && sNode.getIsFirstNode()) ||
                                (keyComp.compare(keySHA, sNode.getPredecessorNode()) == 1 && sNode.getIsFirstNode()) ||
                                (keyComp.compare(keySHA, sNode.getSHA()) == -1 && (keyComp.compare(keySHA, sNode.getPredecessorNode() )==1))) {

//                        if (keySHA.equals(sNode.getSHA()) || (keyComp.compare(keySHA, sNode.getSHA()) == -1 && keyComp.compare(keySHA, sNode.getPredecessorNode()) == 1)) {
                            System.out.println("Inserting-"+ msg.getKey()+"-"+msg.getValue()+" at "+sNode.getPort()+" which was received from "+sNode.getPredecessorPort());
                            try {
                                FileOutputStream outputStream = getContext().openFileOutput(msg.getKey(), Context.MODE_PRIVATE);
                                outputStream.write(msg.getValue().getBytes());
                                outputStream.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        else {
                            System.out.println("Forwarding Insertion-"+ msg.getKey()+"-"+msg.getValue()+"-"+" at "+sNode.getPort()+" to "+sNode.getSuccessorPort()+" which I recieved from "+ sNode.getPredecessorPort());
                            new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "INSERT_REQ", msg.getKey(), msg.getValue(), keySHA, sNode.getSuccessorPort());
                        }
                        continue;
                    }

                    else if (msg.getpType() == PacketType.Query_Req) {
                        String line;
                        String key = msg.getKey();
                        String keySHA = msg.getKeyHash();
                        String value;
                        System.out.println("Query: Request at-"+ sNode.getPort()+"from predecessor-"+sNode.getPredecessorPort()+ " from source " + msg.getSrcPort());
                        RingComparator keyComp = new RingComparator();
                        //System.out.println("Query Request at "+sNode.getPort()+ " from source " + msg.getSrcPort());
                        StringBuffer stringBuffer = new StringBuffer();

                        if ((keySHA.equals(sNode.getSHA())) ||
                                (keyComp.compare(keySHA, sNode.getSHA()) == -1 && sNode.getIsFirstNode()) ||
                                (keyComp.compare(keySHA, sNode.getPredecessorNode()) == 1 && sNode.getIsFirstNode()) ||
                                (keyComp.compare(keySHA, sNode.getSHA()) == -1 && (keyComp.compare(keySHA, sNode.getPredecessorNode() )==1))) {
                            System.out.println("Query: Looking for on current node-"+key);
                            try {
                                //File baseDir = new File(System.getProperty("user.dir") + "data/data/edu.buffalo.cse.cse486586.simpledht/files");
                                //File[] allFiles = baseDir.listFiles();
                                //String absPath = baseDir.getAbsolutePath();
                                //System.out.println("Now reading-"+absPath+"/"+key);
                                //System.out.println("All files are at-" + baseDir.getAbsolutePath());//getContext().getFilesDir());

                                FileInputStream fstream = getContext().openFileInput(key);
                                BufferedReader buffr = new BufferedReader(new InputStreamReader(fstream));

                                while((line =buffr.readLine())!=null) {
                                    stringBuffer.append(line);
                                }
                                buffr.close();
                            } catch (IOException e) {
                                System.out.print("Here is the exception-");
                                e.printStackTrace();
                            }
                            value = stringBuffer.toString();
                            //String[] columnNames = {"key", "value"};
                            //MatrixCursor cur = new MatrixCursor(columnNames);

                            //cur.addRow(new Object[] {key, stringBuffer.toString()});

                            //Log.v("query", selection);
                            // send this cursor to source Node..

                            Socket socket;
                            try {
                                System.out.println("Query: Sending reply message to - " + msg.getSrcPort());
                                socket = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}), Integer.parseInt(msg.getSrcPort()));
                                Packet replymsg = new Packet();
                                replymsg.setKey(key);
                                replymsg.setValue(value);
                                replymsg.setSrcPort(sNode.getPort());
                                replymsg.setpType(PacketType.Query_Ack);
                                ObjectOutputStream ost = new ObjectOutputStream(socket.getOutputStream());
                                ost.writeObject(replymsg);
                                ost.flush();
                                ost.close();
                                socket.close();
                                System.out.println("Query: Reply Message Sent to Source Node.");
                            } catch (UnknownHostException e) {
                                //Log.e(TAG, "ClientTask UnknownHostException");
                                System.out.println("ClientTask UnknownHostException");

                            } catch (IOException e) {
                                System.out.println("ClientTask socket IOException........e.toString()....." + e.toString());
                                //Log.e(TAG, "ClientTask socket IOException..");
                            }

                            //return cur;
                        }
                        else {
                            // forward the request to successor
                            System.out.println ("Query: Message Not Found Locally. Now forwarding " + key+"-" + " to sucessor-" + sNode.getSuccessorPort());
                            new ClientTask().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, "QUERY_REQ", key, keySHA, msg.getSrcPort(), sNode.getSuccessorPort());
                        }
                        continue;
                    }
                    else if (msg.getpType() == PacketType.Query_Ack) {
                        String key = msg.getKey();
                        String value = msg.getValue();
                        System.out.println("Key and Values Received are-"+key+"-"+value);
                        String[] columnNames = {"key", "value"};
                        mcurr = new MatrixCursor(columnNames);
                        mcurr.addRow(new Object[] {key, value});
                        isQueryAcknoweleged = true;
                        continue;
                    }
                    else if (msg.getpType() == PacketType.GlobalQuery) {
                        System.out.println("Query: Received Global Query");
                        String reply = "";
                        String line="";

                        FileInputStream fstream;
                        BufferedReader buffr;
                        System.out.println("***********************Source Port to verify is " + msg.getSrcPort());
                        File baseDir = new File(System.getProperty("user.dir") + "data/data/edu.buffalo.cse.cse486586.simpledht/files");
                        File[] allFiles = baseDir.listFiles();
                        if (allFiles == null) System.out.println("This all files is null");
                        //System.out.println("Number of files are-"+allFiles.length);
                        try {

                            System.out.println("Reading all files at " + sNode.getPort());
                            if (allFiles!=null)
                            for (File f : allFiles) {
                                StringBuffer stringBuffer = new StringBuffer();
                                fstream = getContext().openFileInput(f.getName());
                                buffr = new BufferedReader(new InputStreamReader(fstream));
                                System.out.println("Random Print");
                                while ((line = buffr.readLine()) != null) {
                                    stringBuffer.append(line);
                                }
                                reply = reply + f.getName() + "~" + stringBuffer.toString() + "#";

                                buffr.close();
                            }
                            //System.out.println("\n\nReceived Entire Message is "+msg.getReplyString());
                            //System.out.println("Reply is "+reply);
                            msg.setReplyString(msg.getReplyString() + reply);
                            System.out.println("\n\nEntire Message is " + msg.getReplyString());
                        } catch (IOException e) {
                            System.out.println(e.getStackTrace());
                        }

                        Socket socket;

                        System.out.println ("msg.getSrcPort() and (sNode.getPort()) are-"+ msg.getSrcPort() + " and " + sNode.getPort());
                        if (!msg.getSrcPort().equals(sNode.getPort())) {
                            try {
                                System.out.println("Forwarding Global Query to successor Node*** -" + sNode.getSuccessorPort() + "from"+ sNode.getPort());
                                socket = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}),Integer.parseInt(sNode.getSuccessorPort()));
                                msg.setpType(PacketType.GlobalQuery);
                                //msg.setReplyString("");

                                ObjectOutputStream ost = new ObjectOutputStream(socket.getOutputStream());
                                ost.writeObject(msg);
                                ost.flush();
                                ost.close();
                                socket.close();
                                System.out.println("Query: Reply Message Sent to Successor Node."+sNode.getSuccessorPort());
                            } catch (UnknownHostException e) {
                                System.out.println("ClientTask UnknownHostException");
                                System.out.println(e.getStackTrace());
                            } catch (IOException e) {
                                System.out.println("ClientTask socket IOException........e.toString()....." + e.toString());
                                System.out.println(e.getStackTrace());
                            }
                        } else {
                            System.out.println ("Reached on reflection again..");
                            String[] columnNames = {"key", "value"};
                            mGlobalCurr = new MatrixCursor(columnNames);
                            System.out.println("Received a reply message-" + msg.getReplyString() + " from " + msg.getSrcPort() + "**");
                            String key = "";
                            String value = "";

                            String allMsg = msg.getReplyString();

                            String[] allMsgArr = allMsg.split("#");
                            for (String eachMsg : allMsgArr) {
                                String[] key_val = eachMsg.split("~");

                                if (key_val.length==2 && key_val[0] !=null && key_val[0] != "" && key_val[1]!=null && key_val[1]!="") {
                                    key = key_val[0];
                                    value = key_val[1];
                                    System.out.println("Final Test - Key and Value are-" + key_val[0] + "-and -" + key_val[1]);
                                    mGlobalCurr.addRow(new Object[]{key, value});
                                }
                            }
                            isGlobalQueryAcknoweleged = true;
                            continue;
                        }
                    }
                    else if (msg.getpType() == PacketType.GlobalDelete) {
                        if (msg.getKey().equals("*")) {
                            File baseDir = new File(System.getProperty("user.dir") + "data/data/edu.buffalo.cse.cse486586.simpledht/files");
                            File[] allFiles = baseDir.listFiles();

                            for (File f : allFiles) {
                                f.delete();
                            }
                            continue;
                        }
                        else {
                            //FileInputStream fstream = getContext().openFileInput(msg.getKey());
                            getContext().deleteFile(msg.getKey());
                        }
                    }
                    //ss.close();
                }
            } catch (Exception e) {
                //Log.e(TAG, "Unable to receive message.");
                System.out.println("Unable to receive message due to -" + e.toString());
                return null;
            }
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String[] columnNames = {"key", "value"};
        MatrixCursor cur = new MatrixCursor(columnNames);
        System.out.println("Query is " + selection);
        String line;
        FileInputStream fstream;
        BufferedReader buffr;
        String key = selection;
        System.out.println("Received a *");
        //System.out.println("Ports are-"+sNode.getPort()+"-")
        if ((sNode.getPort().equals(sNode.getPredecessorPort()) && sNode.getPort().equals(sNode.getSuccessorPort())) && (selection.equals("@") || selection.equals("\"@\"") || selection.equals("*") || selection.equals("\"*\"")) ){
            File baseDir = new File(System.getProperty("user.dir") + "data/data/edu.buffalo.cse.cse486586.simpledht/files");
            File[] allFiles = baseDir.listFiles();
            System.out.println("Executing Local Dump QUery");
            try {
                for (File f : allFiles) {
                    StringBuffer stringBuffer = new StringBuffer();
                    fstream = getContext().openFileInput(f.getName());
                    buffr = new BufferedReader(new InputStreamReader(fstream));
                    while ((line = buffr.readLine()) != null) {
                        stringBuffer.append(line);
                    }
                    cur.addRow(new Object[]{f.getName(), stringBuffer.toString()});
                    //System.out.println ("Retrieved " + f.getName()+ "\t--\t" +stringBuffer.toString() );
                    buffr.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return cur;
        }
        else if (sNode.getPort().equals(sNode.getPredecessorPort()) && sNode.getPort().equals(sNode.getSuccessorPort())) {
            StringBuffer stringBuffer = new StringBuffer();

            try {
                fstream = getContext().openFileInput(selection);
                buffr = new BufferedReader(new InputStreamReader(fstream));

                while((line =buffr.readLine())!=null) {
                    stringBuffer.append(line);
                }
                buffr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            cur = new MatrixCursor(columnNames);

            cur.addRow(new Object[] {selection, stringBuffer.toString()});
            Log.v("query", selection);

            return cur;
        }

        if (selection.equals("@") || selection.equals("\"@\"")) {
            File baseDir = new File(System.getProperty("user.dir") + "data/data/edu.buffalo.cse.cse486586.simpledht/files");
            File[] allFiles = baseDir.listFiles();
            System.out.println("Executing Local Dump QUery");
            try {
                for (File f : allFiles) {
                    StringBuffer stringBuffer = new StringBuffer();
                    fstream = getContext().openFileInput(f.getName());
                    buffr = new BufferedReader(new InputStreamReader(fstream));
                    while ((line = buffr.readLine()) != null) {
                        stringBuffer.append(line);
                    }
                    cur.addRow(new Object[]{f.getName(), stringBuffer.toString()});
                    //System.out.println ("Retrieved " + f.getName()+ "\t--\t" +stringBuffer.toString() );
                    buffr.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return cur;
            //Log.v("query", selection);
        } else if (selection.equals("*") || selection.equals("\"*\"")) {
            System.out.println("Executing Global Dump Query");
            System.out.println("Request for global collection from "+sNode.getPort());
            // send message to all nodes to query global files.. ********* Receive part yet to be written...
            Packet msg = new Packet();
            msg.setpType(PacketType.GlobalQuery);
            msg.setSrcPort(myPort);
            msg.setReplyString("");
            msg.setKey("@");
            Socket socket;
            try {
                System.out.println("Global Query Packet sent to " + sNode.getSuccessorPort());
                socket = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}), Integer.parseInt(sNode.getSuccessorPort()));
                ObjectOutputStream ost = new ObjectOutputStream(socket.getOutputStream());
                ost.writeObject(msg);
                socket.close();
            } catch (UnknownHostException e) {
                //Log.e(TAG, "ClientTask UnknownHostException");
                System.out.println("ClientTask UnknownHostException");
            } catch (IOException e) {
                System.out.println("ClientTask socket IOException..");
                //Log.e(TAG, "ClientTask socket IOException..");
            }

            while (!isGlobalQueryAcknoweleged);
            System.out.println ("Coming out of loop of global_query message");
            isGlobalQueryAcknoweleged = false;
            return mGlobalCurr;
        } else {
            System.out.println("Querying Single Key-" + key);
            String keySHA = "";
            try {
                keySHA = genHash(key);
            } catch (NoSuchAlgorithmException e) {
                System.out.println(e.getMessage());
            }
            RingComparator keyComp = new RingComparator();
            System.out.println("Started At "+sNode.getPort()  + " Compare(keySHA, sNodeSHA) gives " + keyComp.compare(keySHA, sNode.getSHA()) + " and keyComp.compare(keySHA, sNode.getPredecessorNode()) gives " + keyComp.compare(keySHA, sNode.getPredecessorNode()));

            if ((keySHA.equals(sNode.getSHA())) ||
                    (keyComp.compare(keySHA, sNode.getSHA()) == -1 && sNode.getIsFirstNode()) ||
                    (keyComp.compare(keySHA, sNode.getPredecessorNode()) == 1 && sNode.getIsFirstNode()) ||
                    (keyComp.compare(keySHA, sNode.getSHA()) == -1 && (keyComp.compare(keySHA, sNode.getPredecessorNode() )==1))) {
                    System.out.println("Query Result found at current node only");
                    try {
                        StringBuffer stringBuffer = new StringBuffer();
                        fstream = getContext().openFileInput(key);
                        buffr = new BufferedReader(new InputStreamReader(fstream));

                        while ((line = buffr.readLine()) != null) {
                            stringBuffer.append(line);
                        }
                        buffr.close();
                        cur.addRow(new Object[]{selection, stringBuffer.toString()});
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }
            else {
                System.out.println ("Message Not Found Locally. Now forwarding " + key+"-" + " to successor-" + sNode.getSuccessorPort());

                Socket socket;
                try {
                    socket = new Socket(InetAddress.getByAddress(new byte[]{10, 0, 2, 2}), Integer.parseInt(sNode.getSuccessorPort()));
                    Packet msg = new Packet();
                    msg.setKey(key);
                    msg.setKeyHash(keySHA);
                    msg.setSrcPort(sNode.getPort());
                    msg.setSuccPort(sNode.getSuccessorPort());
                    msg.setpType(PacketType.Query_Req);
                    ObjectOutputStream ost = new ObjectOutputStream(socket.getOutputStream());
                    ost.writeObject(msg);
                    ost.flush();
                    ost.close();
                    socket.close();
                    System.out.println("CLIENT: Message Forwarded.");
                } catch (UnknownHostException e) {
                    //Log.e(TAG, "ClientTask UnknownHostException");
                    System.out.println("ClientTask UnknownHostException");

                } catch (IOException e) {
                    System.out.println("ClientTask socket IOException........e.toString()....." + e.toString());
                    //Log.e(TAG, "ClientTask socket IOException..");
                }

                System.out.println ("Now going inside loop " + key+"-" + " to successor-" + sNode.getSuccessorPort());

                while (!isQueryAcknoweleged);

                System.out.println ("Coming out of loop " + key+"-" + " to sucessor-" + sNode.getSuccessorPort());
                isQueryAcknoweleged = false;
                return mcurr;
            }
        }
        return cur;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        // TODO Auto-generated method stub
        return 0;
    }

    private String genHash(String input) throws NoSuchAlgorithmException {
        MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
        byte[] sha1Hash = sha1.digest(input.getBytes());
        Formatter formatter = new Formatter();
        for (byte b : sha1Hash) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }

    class RingComparator implements Comparator<Node> {
        public int compare(Node nd1, Node nd2) {
            if (nd1.getSHA().compareTo(nd2.getSHA()) > 0) return 1;
            else return -1;
        }
        public int compare(String keySHA1, String keySHA2) {
            if (keySHA1.compareTo(keySHA2) > 0) return 1;
            else return -1;
        }
    }
}