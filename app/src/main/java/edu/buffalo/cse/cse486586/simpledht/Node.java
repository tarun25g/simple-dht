package edu.buffalo.cse.cse486586.simpledht;

/**
 * Created by tarun on 3/27/15.
 */
public class Node {
    private String port;
    private String successorNode;            // Keeps SHA of Sucessor Node
    private String predecessorNode;         // Keeps SHA of Predecessor Node
    private String SHA;
    private Boolean isFirstNode;
    private String successorPort;
    private String predecessorPort;

    public void setPredecessorPort(String predecessorPort) {
        this.predecessorPort = predecessorPort;
    }
    public void setSuccessorPort(String successorPort) {
        this.successorPort = successorPort;
    }
    public String getPredecessorPort() {
        return predecessorPort;
    }
    public String getSuccessorPort() {
        return successorPort;
    }

    public void setPort(String port) {
        this.port = port;
    }
    public String getPort() {
        return port;
    }

    public void setSuccessorNode(String successorNode) {
        this.successorNode = successorNode;
    }
    public String getSuccessorNode() {
        return successorNode;
    }

    public void setPredecessorNode(String predecessorNode) {
        this.predecessorNode = predecessorNode;
    }
    public String getPredecessorNode() {
        return predecessorNode;
    }

    public void setSHA(String SHA) {
        this.SHA = SHA;
    }
    public String getSHA() {
        return SHA;
    }

    public void setIsFirstNode(Boolean isFirstNode) {
        this.isFirstNode = isFirstNode;
    }
    public Boolean getIsFirstNode() {
        return isFirstNode;
    }
}



